# Unisport enrollment addition
This add-on to Firefox makes enrollment to classes a bit easier.

I have no ties to [Unisport](https://unisport.fi/). This addition is provided as-is.

Icon from [feathericons](https://github.com/feathericons/feather)
